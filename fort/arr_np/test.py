#!python3 -u

from ctypes import cdll
from ctypes import POINTER, byref, c_int, c_long, c_bool, c_char_p, c_float, c_void_p, c_double, cast
import numpy as np

lib = cdll.LoadLibrary('./main.so')

coords = np.arange(3*11,dtype=np.float64).reshape((11,3))

lib.set_coords(coords.ctypes.data_as(c_void_p), coords.shape[0])
lib.print_coords()

