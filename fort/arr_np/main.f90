module mymod
use iso_c_binding

IMPLICIT NONE

integer, parameter :: coords_prec = 8

real(kind = coords_prec), dimension(:,:), pointer:: coords

contains

subroutine set_coords(c, n_atoms) bind(c)
  ! set memory for coords and its size
  type(c_ptr),value :: c
  integer(c_int), value :: n_atoms
  call c_f_pointer(c, coords, [3, n_atoms])
end subroutine set_coords


subroutine print_coords() bind(c)
  ! print coords
  integer :: i
  do i = 1,size(coords,2)
    print *, i, coords(:,i)
  end do
end subroutine print_coords

end module mymod

