module mymod
use iso_c_binding

IMPLICIT NONE

integer, parameter :: forces_prec = 4

integer, bind(c) :: n_atoms
real(kind = forces_prec), dimension(:,:), allocatable, target :: total_forces
real(kind = forces_prec), dimension(:,:), pointer :: half_forces

contains

subroutine alloc_forces(n_atoms_arg) bind(c)
  ! allocate memory for total_forces and fill it with smth
  integer(c_int), value :: n_atoms_arg
  integer :: info
  n_atoms = n_atoms_arg
  allocate(total_forces(3, n_atoms),stat=info)
  
  half_forces => total_forces(:2,:n_atoms/2)
  
end subroutine alloc_forces

type(c_ptr) function get_total_forces() bind(c)
  ! return pointer to total_forces
  get_total_forces = c_loc(total_forces(1,1))
end function get_total_forces


subroutine get_2darray_params(a, sz1, sz2, st1, st2)
  ! for paranoics
  ! calclate size (sz1, sz2) and memory layout (strides st1,st2) of 2d array a
  real, target, intent(in) :: a(:,:)
  integer, intent(out) :: sz1,sz2, st1,st2
  integer(kind=c_intptr_t) :: p11, p12, p21

  sz1 = size(a,1)
  sz2 = size(a,2)
  p11 = TRANSFER(c_loc(a(1,1)), p11)
  p12 = TRANSFER(c_loc(a(1,2)), p12)
  p21 = TRANSFER(c_loc(a(2,1)), p21)
  st1 = p21 - p11
  st2 = p12 - p11
end subroutine get_2darray_params

subroutine get_total_forces_array_params(sz1, sz2, st1, st2) bind(c)
  ! for paranoics
  ! return total_forces size and memory layout
  integer, intent(out) :: sz1,sz2, st1,st2
  call get_2darray_params(total_forces, sz1, sz2, st1, st2)
end subroutine get_total_forces_array_params

subroutine print_total_forces() bind(c)
  ! print total_forces
  integer :: i
  do i = 1,size(total_forces, 2)
    print *, i, total_forces(:,i)
  end do

  print *,'Half'
  do i = 1,size(half_forces, 2)
    print *, i, half_forces(:,i)
  end do
end subroutine print_total_forces


subroutine get_half_forces_array_params(sz1, sz2, st1, st2) bind(c)
  ! Not just for paranoics !
  ! return half_forces size and memory layout
  integer, intent(out) :: sz1,sz2, st1,st2
  call get_2darray_params(half_forces, sz1, sz2, st1, st2)
end subroutine get_half_forces_array_params

type(c_ptr) function get_half_forces() bind(c)
  ! return pointer to total_forces
  get_half_forces = c_loc(half_forces(1,1))
end function get_half_forces

end module mymod

