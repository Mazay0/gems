#!python3 -u

from ctypes import cdll
from ctypes import POINTER, byref, c_int, c_float
import numpy as np

lib = cdll.LoadLibrary('./main.so')

lib.alloc_forces.argtypes = [c_int]
lib.alloc_forces(11)
n_atoms = c_int.in_dll(lib, 'n_atoms').value

forces_type = c_float

 # beleive that total_forces is contiguous
lib.get_total_forces.restype = POINTER(forces_type)
forces_view = np.ctypeslib.as_array(lib.get_total_forces(), shape=(n_atoms, 3))

# set via Python
forces_view[:,:] = np.arange(n_atoms*3,dtype=forces_type).reshape((n_atoms,3))

lib.print_total_forces()    #print via Fortran
print (f'{forces_view = }') #print via Python


#================== stuff for non-contiguous arrays ===============

def get_array_params(func):
  sz1 = c_int()
  sz2 = c_int()
  st1 = c_int()
  st2 = c_int()

  func.argtypes = [POINTER(c_int), POINTER(c_int), POINTER(c_int), POINTER(c_int)]
  func(sz1,sz2,st1,st2)
  return (sz2.value, sz1.value), (st2.value, st1.value) # reverse Fortran order (column-major)

def get_array_view(params_func, pointer_func, dtype):
  def buf_size(shape, strides):
    offs = (np.array(shape)-1)*np.array(strides)
    lower = sum(offs[offs < 0])
    upper = sum(offs[offs > 0])
    return upper - lower
    
  shape, strides = get_array_params(params_func)
  pointer_func.restype = POINTER(dtype*buf_size(shape, strides))
  return np.ndarray(shape, dtype = dtype, buffer=pointer_func().contents, strides=strides)

half_forces_view = get_array_view(lib.get_half_forces_array_params, lib.get_half_forces, forces_type)
print (f'{half_forces_view = }')

half_forces_view[:,:] = 100*np.arange(np.product(half_forces_view.shape),dtype=forces_type).reshape(half_forces_view.shape)

lib.print_total_forces()    #print via Fortran
print (f'{forces_view = }') #print via Python

