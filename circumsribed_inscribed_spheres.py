def xdet(a):
	"""
		Exact determinat via https://en.wikipedia.org/wiki/Laplace_expansion
	"""
	if a.shape == (1,1):
		return a[0,0]
	elif a.shape == (2,2):
		return a[0,0]*a[1,1] - a[0,1]*a[1,0]
	elif a.shape == (3,3):
		return a[0,0]*a[1,1]*a[2,2] + a[0,1]*a[1,2]*a[2,0] + a[0,2]*a[1,0]*a[2,1] - a[0,2]*a[1,1]*a[2,0] - a[0,1]*a[1,0]*a[2,2]  - a[0,0]*a[1,2]*a[2,1]
	else:
		return np.sum([a[i,0]*cofactor(a,i,0) for i in range(a.shape[0])])

def minor(a, i, j):
	ii = list(range(0,i)) + list(range(i+1,a.shape[0]))
	jj = list(range(0,j)) + list(range(j+1,a.shape[1]))
	b = a[np.ix_(ii,jj)]
	return xdet(b)

def cofactor(a, i=None, j=None):
	"""
		Return cofactor matrix or single cofactor element if indices are specified
	"""
	if i is not None and j is not None:
		return ((-1)**(i+j)) * minor(a,i,j)

	b = np.zeros(a.shape, a.dtype)
	for i,j in np.ndindex(a.shape):
		b[i,j] = cofactor(a,i,j)
	return b

def adj(a):
	return cofactor(a).T

def xinv(a):
	"""
		Exact inversion
	"""
	return xdet(a), adj(a)


def cell_circumscribed_diameter(a):
	assert a.shape==(3,3), a.shape
	diagonals = [[1,1,1], [-1,1,1], [1,-1,1], [1,1,-1]]
	diagonals = diagonals @ a
	assert diagonals.shape == (4,3)
	diagonals_lengths = np.linalg.norm(diagonals, axis=1)
	return np.max(diagonals_lengths)

def cell_inscribed_diameter(a):
	assert a.shape==(3,3), a.shape
	V = abs(xdet(a))
	Bmax = np.max([np.linalg.norm(np.cross(x,y)) for x,y in [(a[0],a[1]),(a[0],a[2]),(a[1],a[2])]])
	hmin = V / Bmax
	return hmin

