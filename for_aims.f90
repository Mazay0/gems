
subrotuine TMP_LHS_info(loc_LHS, sc_desc_LHS)
   real(dp), intent(in) :: loc_LHS(:,:)
   integer, intent(in) :: sc_desc_LHS(DLEN_)

   integer :: nprow, npcol, myprow, mypcol, context
   real(dp), allocatable x(:,:)
  
   context = sc_desc_LHS(CTXT_)
   call BLACS_GRIDINFO( context, nprow, npcol, myprow, mypcol )
   ! create temporary matrix
   loc_x_rows =  NUMROC(N, desc_b(MB_), myprow, sc_desc_LHS(RSRC_), nprow)
   loc_x_cols =  NUMROC(N, desc_b(MB_), myprow, sc_desc_LHS(RSRC_), nprow)
   call DESCINIT( desc_x, N, NRHS, desc_b(MB_), desc_b(NB_), &
            desc_b(RSRC_), desc_b(CSRC_), context, loc_x_rows, info )
   call aims_allocate(x, loc_x_rows, size(b,2), name="x")

   ! x <- U^T * b
   call PDGEMM("T", "N", N, NRHS, M, ONE, U, 1, 1, desc_U, b, 1, 1, desc_b, &
         ZERO, x, 1, 1, desc_x)
end subrotuine
subroutine save_array(ofn, A)
   use types, only: dp
   character(*), intent(in) :: ofn
   real(dp), intent(in)  ::  A(:)
  
   call save_matrix(ofn, reshape(A, (/size(A), 1/)))
end subroutine

subroutine save_matrix(ofn, A)
   use types, only: dp
   character(*), intent(in) :: ofn
   real(dp), intent(in)  ::  A(:,:)
  
   integer :: fout, ios
   integer :: i, j

  print *, "Saving matrix ", size(A, 1), size(A,2), " into ", ofn
  open(newunit=fout, file=ofn, action="write", iostat=ios)
  if (ios /= 0) stop 'Could not open file in save_matrix()'
  
  do i=1,size(A, 1)
    do j=1,size(A, 2)
      write(fout, '(G20.8e3)', advance="no") A(i,j)
    enddo
    write(fout, *)
  enddo
  close(fout)
end subroutine

subroutine load_matrix(fn, A)
   use types, only: dp
   character(*), intent(in) :: fn
   real(dp), intent(out)  ::  A(:,:)
  
   integer :: fin, ios
   integer :: i, j

  print *, "Loading matrix ", size(A, 1), size(A,2), " from ", fn
  open(newunit=fin, file=fn, action="read", iostat=ios)
  if (ios /= 0) stop 'Could not open file in load_matrix()'
  
  do i=1,size(A, 1)
    do j=1,size(A, 2)
      read(fin, *) A(i,j)
    enddo
  enddo
  close(fin)
end subroutine

function get_volume_pbc_mc(mpi_comm) result(volume)
   ! Computes cavity volume in a periodic system based on get_abs_cavfunc_and_abs_grad()
   use mpe_dielectric_interfaces_common, only: &
         charlen_for_if_gen, &
         cross_d3
   use numerical_utilities, only:&
      twonorm
   use geometry, only:&
      lattice_vector
   use synchronize_mpi_basic, only:&
      sync_real_number
   use isc_cavity_function, only:&
      get_abs_cavfunc_and_abs_grad

   integer, intent(in), optional :: mpi_comm
   integer, parameter :: M = 20000000
   integer, parameter :: K = 400
   real(dp) :: volume
   real(dp) :: points(3, M), cart_points(3, M)
   real(dp) :: cavfunc(M), cavfunc_grad(3, M)

   integer :: mpi_rank, mpi_size, mpi_ierr
   integer :: i
   if (present(mpi_comm)) then
      call MPI_Comm_rank(mpi_comm, mpi_rank, mpi_ierr)
      call MPI_Comm_size(mpi_comm, mpi_size, mpi_ierr)
   else
      mpi_rank = 0
      mpi_size = 1
   endif
   
   volume = 0.0
   do i=1,K
		 call random_number(points(1,:))
		 call random_number(points(2,:))
		 call random_number(points(3,:))
		 
		 cart_points = matmul(lattice_vector, points)
		 call get_abs_cavfunc_and_abs_grad(M, cart_points, cavfunc, cavfunc_grad)
		 volume = volume + count(cavfunc > 0) / real(M,dp)
		 print *, "volume # ", i, volume / i
	 enddo
	 
	 volume = volume / K
   
   print *, "volume rel ", volume
   volume = volume * abs(determinant_3x3(lattice_vector)) ! absolute volume
   print *, "volume abs ", volume

   call sync_real_number(volume)
   volume = volume / mpi_size
   print *, "volume sync ", volume
   
   if (.not. IEEE_IS_NORMAL(volume)) then
      call aims_stop("Error: MPE volume is NaN or Infinity in get_volume_pbc")
   endif
end function get_volume_pbc_mc
