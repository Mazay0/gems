#!/bin/sh

# See also
# https://github.com/clonker/mc-recipe/blob/master/build.sh


git clone https://github.com/MidnightCommander/mc mc
cd mc
git checkout tags/4.8.9
./autogen.sh
mkdir prefix
./configure --without-x --prefix=`pwd`/prefix --with-screen=ncurses
make -j 4
make install

# add in .bashrc:
export PATH=${HOME}/bin:${PATH}
export MC_DATADIR=${HOME}/etc/mc/


# for static build with ncurses and glib


wget https://github.com/ninja-build/ninja/releases/download/v1.11.0/ninja-linux.zip
unzip ninja-linux.zip
mv ninja ~/bin/
. ~/.bashrc

~/ve/bin/pip install meson
~/ve/bin/meson setup --prefix=`pwd`/prefix --default-library=static _build
~/ve/bin/meson compile -C _build
~/ve/bin/meson install -C _build

# build glib: https://docs.gtk.org/glib/building.html
~/ve/bin/meson setup --prefix=`pwd`/prefix --default-library=static _build
~/ve/bin/meson compile -C _build
~/ve/bin/meson install -C _build
export PKG_CONFIG_PATH=`pwd`/glib/prefix/lib64/pkgconfig:$PKG_CONFIG_PATH

# build ncurses
git clone https://github.com/mirror/ncurses/ ncurses
git checkout tags/v5.9
#CPPFLAGS="-P" ./configure --prefix=`pwd`/prefix --without-cxx-binding --enable-termcap --with-normal --with-shared --with-libtool --enable-rpath
CPPFLAGS="-P" ./configure --prefix=`pwd`/prefix --without-cxx-binding --enable-termcap --with-normal --without-shared
make 
make install


GLIB_LIBDIR=`pwd`/glib/prefix/lib64/ ./configure --without-x --prefix=`pwd`/prefix --with-screen=ncurses --with-glib-static --with-ncurses-includes=`pwd`/ncurses/prefix/include --with-ncurses-libs=`pwd`/ncurses/prefix/lib --disable-shared --enable-static
