# gems

Solutions

# bsum.py

Blockwise reduction (`sum` by default) of `ndarray` along specific axis 
```
print (bsum(a, [1,3], -1))
print (bsum(a, [1,3], -1, np.multiply))
print (bsum(a, [1,3], -1, np.maximum))
```

# center_ac.py

Place adsorption complex (AC) in the slab center. 

Works for SuSMoST single-AC samples.

# get_blacs_lib_ctypes.py

Looks for the BLACS library among loaded libraries of the current process.

# loadtxt.c

Loads complex array from the text file created by NumPy's `savetxt()`

# make-mc.sh

Build Midnight Commander from source

# pbc_clustering.ipynb

Clusterize atoms into molecules using DBSCAN with PBC

# plot.py

Plot data from console using matplotlib. Simple command line tool

# pycur.py

Snippet for using ncurses from Python via ctypes

# lammps_go.py

Snippet for using LAMMPSrun calculators with ZBL potential

# zbl.py

ZBL potential evaluation with GROMACS switching function. Used LAMMPS as a reference


