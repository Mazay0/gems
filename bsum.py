import numpy as np

a = np.arange(12).reshape((3,4))

def slice_along(ndim, axis, s):
  r = [np.s_[:],] * ndim
  r[axis] = s
  return tuple(r)

def bsum(a, b, axis, ufunc=np.add):
  '''
    Blockwise reduce along axis
    a: array
    b: block sizes along `axis`, `a.shape[axis] == sum(b)`
    axis:
  '''
  assert a.shape[axis] == sum(b)
  cum_ranges = [0] + list(np.cumsum(b))
  ndim = len(a.shape)
  n = len(b)  # new size along axis
  
  nshape = list(a.shape) # new shape
  nshape[axis] = n
  nshape = tuple(nshape)
  
  r = np.zeros(nshape, dtype=a.dtype)
  for i in range(n):
    sr = slice_along(ndim, axis, i)
    sa = slice_along(ndim, axis, range(cum_ranges[i], cum_ranges[i+1]))
    r[sr] = ufunc.reduce(a[sa], axis=axis)
  return r

print (a)
print (bsum(a, [1,3], -1))
print (bsum(a, [1,3], -1, np.multiply))
print (bsum(a, [1,3], -1, np.maximum))
