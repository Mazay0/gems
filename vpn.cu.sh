#!/bin/sh

export OPENSSL_CONF=~/sshs/cu.ssl.conf

# Enable UnsafeLegacyRenegotiation for Debian Testing.
# Links for details:
# https://github.com/dlenski/gp-saml-gui/issues/37
# https://bugs.launchpad.net/ubuntu/+source/openssl/+bug/1963834/comments/6
# https://stackoverflow.com/questions/71603314/ssl-error-unsafe-legacy-renegotiation-disabled

cat > $OPENSSL_CONF << EOF

openssl_conf = openssl_init

[openssl_init]
ssl_conf = ssl_sect

[ssl_sect]
system_default = system_default_sect

[system_default_sect]
Options = UnsafeLegacyRenegotiation
EOF

# Workaround for a blank browser window
# https://forums.debian.net/viewtopic.php?t=157612
# https://github.com/dlenski/gp-saml-gui/issues/91
export WEBKIT_DISABLE_DMABUF_RENDERER="1"

. ~/sshs/ve-gp-saml-gui/bin/activate

gp-saml-gui -S -v -C ~/sshs/cookies-vpn.cu  -g ras.cf.ac.uk


