from subprocess import run
from psutil import Process
from time import sleep

def runcmd(*args):
  p = run([*args], capture_output=True)
  p.check_returncode()
  lines = p.stdout.decode("utf-8").split('\n')
  if lines[-1]=='':
    lines = lines[:-1]
  return lines
  
def get_active_wid():
  lines = runcmd("xprop", "-root")
  wid = [l.split()[4] for l in lines if '_NET_ACTIVE_WINDOW(WINDOW)' in l][0]
  return int(wid[2:], 16)

def get_windows():
  lines = runcmd("wmctrl", "-lp")
  for l in lines:
    l = l.split(maxsplit=4)
    wid = int(l[0][2:], 16)
    pid = int(l[2])
    exename = Process(pid).name()
    title = l[4]
    yield (wid, pid, exename, title)

def get_active_window():
  awid = get_active_wid()
  for wid, pid, exename, title in get_windows():
    if wid==awid:
      return (exename, title)


def is_screen_locked():
  res = runcmd(*'gdbus call -e -d org.gnome.SessionManager -o /org/gnome/SessionManager/Presence -m org.freedesktop.DBus.Properties.Get /org/gnome/SessionManager/Presence status'.split())[0]
  return int(res[9]) > 0

while True:
  if not is_screen_locked():
    print (get_active_window())
  sleep(1)
  
  
