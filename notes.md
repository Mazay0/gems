# DFTB+

??? See MPI version below ??? FC=gfortran CC=gcc cmake  -DBUILD_SHARED_LIBS=1 -DENABLE_DYNAMIC_LOADING=1 -DLAPACK_LIBRARY=openblas -DCMAKE_INSTALL_PREFIX=$HOME/opt/dftbp-nompi -B _build-nompi . # add  -DWITH_PYTHON=1 for pyapi

cmake --build _build-nompi -- -j 4

./utils/get_opt_externals slakos

pushd _build-nompi; ctest -j; popd

OR:

pushd _build-nompi-py; ctest -R pyapi_*; popd

DESTDIR=/ cmake --install _build-nompi

OR:

DESTDIR=/ make install


LDFLAGS="-Wl,--copy-dt-needed-entries" CMAKE_PREFIX_PATH=/usr/lib64/openmpi FC=/usr/lib64/openmpi/bin/mpif90 CC=/usr/lib64/openmpi/bin/mpicc cmake -DWITH_MPI=1 -DBUILD_SHARED_LIBS=1 -DENABLE_DYNAMIC_LOADING=1 -DBLAS_LIBRARY=openblas -DLAPACK_LIBRARY=openblas -DSCALAPACK_LIBRARY=scalapack -DCMAKE_INSTALL_PREFIX=$HOME/opt/dftbp-mpi -B _build-mpi .

cmake --build _build-mpi -- -j 4

pushd _build-mpi; ctest -j; popd

cmake --install _build-mpi


###Note:
`LDFLAGS="-Wl,--copy-dt-needed-entries"` is necessary to avoid "DSO missing from command line" error with undefined reference to pdgemr2d_. This error is due to improper order of libraries linsting in linker's command line. As we can't control that order for DFTB+ building scripts, we can force linker to recursively search symbols. Details: <https://stackoverflow.com/a/8725914>


# FHI-aims

```
export CI_JOB_NAME=build-so-GOO
AIMS_HOME=${PWD}/$CI_JOB_NAME/install/  ./ci_scripts/build_aims.sh ${PWD}/cmake/toolchains/libaims.GOO.cmake $CI_JOB_NAME 4
```

## Testing

```
cd ~/prg/aims
python3 -m venv ve
./ve/bin/python3 -m pip install mpi4py asi4py
export CI_JOB_NAME=build-so-GOO
TESTING_PYTHON=${PWD}/ve/bin/python3 ./ci_scripts/test_aims.sh $CI_JOB_NAME /usr/lib64/openmpi/bin/mpiexec 4 5
```

# NumPy

Details on Building from source: <https://numpy.org/doc/stable/user/building.html>

pip cache purge

pip install --upgrade pip

```
pip install XXX --no-clean # do not remove temporary files
```

```
unset MKLROOT # it is necessary because if it is there, then numpy.linalg will have libmkl_rt.so reference
MAKEFLAGS="-j8" NPY_BLAS_ORDER=openblas pip install numpy --no-cache-dir --no-binary numpy
```

Check build options:

`python3 -c 'import numpy as np; np.show_config()'`

# Scalapack

```
mkdir _build
cd _build

FFLAGS="-fallow-argument-mismatch" CC=~/prg/openmpi-4.1.5/install/bin/mpicc FC=~/prg/openmpi-4.1.5/install/bin/mpif90 cmake -DBUILD_SHARED_LIBS=True -DCMAKE_INSTALL_PREFIX=`pwd`/../install build ../scalapack_build.cmake ..

make -j 4 install

```

## Check preformance:

1. `python3 -c 'import numpy as np; np.show_config(); print(np)'`
2. `OMP_NUM_THREADS=4 python3 -m timeit 'import numpy as np; n = 3000; A = np.random.random((n, n)); B = A**0.5; C = A @ B'`

Samples:
* `1 loop, best of 5: 1.49 sec per loop` for `Intel(R) Core(TM) i5-6500 CPU @ 3.20GHz` with OpenBLAS
* `10 loops, best of 3: 827 msec per loop` for Archer2 `AMD EPYC 7742 64-Core` with OpenBLAS
* `1 loop, best of 5: 1.16 sec per loop` for GreyPatch `Intel(R) Xeon(R) CPU X5687  @ 3.60GHz` with openblas-pthread
* `1 loop, best of 5: 444 msec per loop` for Hawk chemy3 `AMD EPYC 7502 32-Core Processor` with libopenblasp-r0-34a18dc3.3.7.so
* `1 loop, best of 5: 746 msec per loop` for Hawk dev `Intel(R) Xeon(R) Gold 6148 CPU @ 2.40GHz` with libopenblasp-r0-34a18dc3.3.7.so
* `1 loop, best of 5: 658 msec per loop` for Hawk dev `Intel(R) Xeon(R) Gold 6148 CPU @ 2.40GHz` with intel2020u1/libmkl_rt.so

# SciPy

Error on `pip install scipy --no-binary scipy`:

```
...
 ../../scipy/meson.build:134:0: ERROR: Dependency "OpenBLAS" not found, tried pkgconfig and cmake
...
```

This is due to lack of PkgConf for OpenBLAS on Fedora.

Use older scipy version:

```
pip install scipy==1.8.1 --no-cache-dir  --no-binary scipy
```


# ELPA

`wget "https://elpa.mpcdf.mpg.de/software/tarball-archive/Releases/2019.11.001/elpa-2019.11.001.tar.gz"`

```
./configure CC=gcc-10 FC=gfortran-10 FCFLAGS='-fallow-argument-mismatch' CFLAGS='-march=native -mtune=native' --enable-openmp --prefix=`pwd`/install --with-mpi=no  --disable-avx  --disable-avx2 --disable-avx512
```

`make -j 4 install`




# GPAW

See <https://wiki.fysik.dtu.dk/gpaw/install.html#siteconfig>

`pip install gpaw --no-cache-dir --no-binary gpaw`

Check linkage:
`ldd ve/lib/python3.9/site-packages/_gpaw.cpython-39-x86_64-linux-gnu.so`

`OMP_NUM_THREADS=1 mpiexec -n 4 python3 -c 'from gpaw import GPAW; from ase.build import molecule; a = molecule("C6H6"); a.center(vacuum=5); a.calc=GPAW(mode="lcao", xc="PBE", parallel={"sl_auto":True}); a.get_potential_energy()'`



# GlobalProtect-openconnect

1. `sudo apt install gp-saml-gui`
2. Run `gp-saml-gui -g ras.cf.ac.uk`
3. Login/password/MSauthViaPhone
4. Copy/paste/run output from console: 
```
echo EMhk5shRKETq8Jc2Zfn3OiaEwcqj/6xcFYlPOaOrpuxleCM2eE2BnnpgBmUuq9QL |
        sudo openconnect --protocol=gp --user=loginX --os=linux-64 --usergroup=gateway:prelogin-cookie --passwd-on-stdin ras.cf.ac.uk
```

OR

2. Run `gp-saml-gui -S -g ras.cf.ac.uk`
3. Login/password/MSauthViaPhone




# LAMMPS

rm -r build; mkdir build && cd build && LDFLAGS="-Wl,--copy-dt-needed-entries" cmake ../cmake -D WITH_JPEG=yes -D WITH_PNG=yes -D WITH_GZIP=yes -D PKG_COMPRESS=yes -D PKG_CORESHELL=yes -D PKG_DIPOLE=yes -D PKG_DRUDE=yes -D PKG_KIM=yes -D PKG_KSPACE=yes -D PKG_MC=yes -D PKG_MGPT=yes -D PKG_MOLECULE=yes -D PKG_PLUGIN=yes  -D PKG_REPLICA=yes -D PKG_SMTBQ=yes -D PKG_OPT=yes -D PYTHON_EXECUTABLE=/usr/bin/python3 -D PKG_REAXFF=yes  -D PKG_QEQ=yes -D PKG_MANYBODY=yes  -D PKG_OPENMP=yes -D PKG_MEAM=yes -D PKG_MISC=yes -D PKG_COLVARS=yes -D PKG_PLUMED=yes -D DOWNLOAD_PLUMED=yes -D PKG_H5MD=yes -D PKG_NETCDF=yes  -D PKG_ML-QUIP=yes -D QUIP_LIBRARY=${HOME}/prg/ve-gap/lib/python3.9/site-packages/quippy/libquip.a -D CMAKE_INSTALL_PREFIX=$HOME/opt/lammps && make -j 4 install

### v2

LDFLAGS="-Wl,--copy-dt-needed-entries" cmake ../cmake -D WITH_JPEG=yes -D WITH_PNG=yes -D WITH_GZIP=yes -D PKG_COMPRESS=yes -D PKG_CORESHELL=yes -D PKG_DIPOLE=yes -D PKG_DRUDE=yes -D PKG_KIM=yes -D PKG_KSPACE=yes -D PKG_MC=yes -D PKG_MGPT=yes -D PKG_MOLECULE=yes -D PKG_PLUGIN=yes  -D PKG_REPLICA=yes -D PKG_SMTBQ=yes -D PKG_OPT=yes -D PYTHON_EXECUTABLE=/usr/bin/python3.9 -D PKG_REAXFF=yes  -D PKG_QEQ=yes -D PKG_MANYBODY=yes  -D PKG_OPENMP=yes -D PKG_MEAM=yes -D PKG_MISC=yes -D PKG_COLVARS=yes -D PKG_PLUMED=yes -D DOWNLOAD_PLUMED=yes -D PKG_H5MD=yes -D PKG_NETCDF=yes  -D PKG_ML-QUIP=yes -D QUIP_LIBRARY=${HOME}/prg/ve/lib/python3.9/site-packages/quippy/libquip.a -D BUILD_SHARED_LIBS=yes -D CMAKE_INSTALL_PREFIX=$HOME/opt/lammps2 && make -j 8 install

# Docker

## Launch image with shell history and mounted local repo

`sudo docker run  -e HISTFILE=/code/.hist -v /home/user/prg/code:/code -it user/dev-img  /bin/bash`


# Low-rank matrix decomposition

1. CUR https://en.wikipedia.org/wiki/CUR_matrix_approximation
2. CSSP - Column subset selection problem. RRQR gives CSSP: $A\Pi = QR$ => $C = A\Pi_k$
  * https://arxiv.org/pdf/1606.06511.pdf
3. Nystr%C3%B6m approximation 
  * https://en.wikipedia.org/wiki/Low-rank_matrix_approximations#Nystr%C3%B6m_approximation
  * https://arxiv.org/pdf/2002.09073.pdf

# Git

## List all files in current directory

`git ls-files -otc`

## Submodules

* List submodules with versions and commit id's: `git sumbodule status`
* Compare with other branch (e.g. `origin/master`): `git difftool -d origin/master`
* Select commit for submodule (e.g. `esl/elsi_interface/`), ONLY in the working copy: `cd esl/elsi_interface/ && git checkout ce1b50a832c8f886a69e43975e0def15beee22ef`
  - Save the selected submodule commit: in the project dir (NOT in submodule): `git commit -m 'Updated a submodule'``
  
  
# Core dumps

## Check Core dumping command
`sudo sysctl kernel.core_pattern`

or 

`cat /proc/sys/kernel/core_pattern`

## Set Core dumping command

`sudo sysctl -w kernel.core_pattern=/tmp/core-%e.%p.%h.%t`

## Core Dump in Docker

In docker `/proc/sys/kernel/core_pattern` is readonly, because it is inherited from host OS.So it should be setted in host.

# Set terminal in Cinamon

`gsettings set org.cinnamon.desktop.default-applications.terminal exec /usr/bin/konsole`

`sudo update-alternatives --config x-terminal-emulator`

# SURM

## Launching MPI task in interactive session:

```
# launching session:
srun -p c_compute_amd --account=scw10XX --time=24:0:0 --ntasks 1 --cpus-per-task 64 --pty bash

# within session:
srun --overlap -n 64 python3 -c 'print(123)'
```

# sixel in XTerm

To enable sixel suppor in XTerm add follwoing in `~/.Xresources`:
```
XTerm*decTerminalID: vt340
XTerm*numColorRegisters: 256
XTerm*disallowedWindowOps: 1,2,3,4,5,6,7,8,9,11,13,19,20,21,GetSelection,SetSelection,SetWinLines,SetXprop


xterm*faceName: Monospace
xterm*faceSize: 14

xterm*VT100.Translations: #override \
                 Ctrl Shift <Key>V:    insert-selection(CLIPBOARD) \n\
                 Ctrl Shift <Key>C:    copy-selection(CLIPBOARD)

xterm*background: black
xterm*foreground: white

xterm*maxStringParse: 10000000
xterm*maxGraphicSize: 10000x10000

```

And reload it:
```
xrdb -merge ~/.Xresources
```

Then restart XTerm. 

Use tmux >= 3.4-3 for sixel support.

# Editable package installation with pip:

```
cd ~/prg/asi/
MPICC=/lib64/openmpi/bin/mpicxx   pip install -e ./pyasi/
```
or from here: https://gitlab.com/FHI-aims-club/pyfhiaims/-/blob/main/.gitlab-ci.yml
```
pip install -e .[tests,dev] --verbose
```