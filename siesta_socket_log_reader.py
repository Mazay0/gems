import numpy as np

def load_siesta_socket_log(f):
  res = []
  for l in f:
    if "%block AtomicCoordinatesAndAtomicSpecies" in l:
      inSpecies = []
      for l in f:
        l = l.strip()
        if l == '%endblock AtomicCoordinatesAndAtomicSpecies':
          break
        inSpecies.append(l.split()[-1])
      inSpecies = np.array(inSpecies).astype(float)
    elif 'coordsFromSocket cell (bohr) =' in l:
      inCell = np.array([f.readline().split() for i in range(3)]).astype(float)
    elif 'coordsFromSocket coords (bohr) =' in l:
      inCoords = []
      for l in f:
        l = l.strip()
        if l == '':
          break
        inCoords.append(l.split())
      inCoords = np.array(inCoords).astype(float)
    elif 'forcesToSocket energy (hartree) =' in l:
      outE = float(l[l.find('=')+1:])
    elif 'forcesToSocket stress (hartree/bohr^3) =' in l:
      outStress = np.array([f.readline().split() for i in range(3)]).astype(float)
    elif 'forcesToSocket forces (hartree/bohr) =' in l:
      outForces = np.array([f.readline().split() for i in range(len(inCoords))]).astype(float)
      res.append((inSpecies, inCell, inCoords, outE, outStress, outForces))
  return res

import sys
from ase.io.trajectory import TrajectoryWriter
from ase import Atoms

res = load_siesta_socket_log(open(sys.argv[1],'r'))

tw = TrajectoryWriter(sys.argv[2])

for a in res:
  print (a[0].shape, a[1].shape, a[2].shape, a[3], a[4].shape ,a[5].shape)
  atoms = Atoms(numbers = a[0], cell=a[1], positions=a[2])
  tw.write(atoms, energy=a[3], stress=a[4], forces=a[5])
