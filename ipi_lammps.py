import sys, csv, os
import numpy as np
from time import time
from ase import Atoms
from ase.build import bulk, make_supercell
from ase.optimize.lbfgs import LBFGSLineSearch, LBFGS
from ase.constraints import ExpCellFilter, StrainFilter
from ase.stress import voigt_6_to_full_3x3_strain, full_3x3_to_voigt_6_strain, full_3x3_to_voigt_6_stress
from ase import units
from ase.units import GPa
from ase.io import read, write
from ase.io.trajectory import TrajectoryWriter

from ase.calculators.lammpsrun import LAMMPS
from ase.calculators.socketio import SocketIOCalculator
import multiprocessing as mp


from ase.data import atomic_numbers, chemical_symbols, atomic_masses


os.environ['ASE_LAMMPSRUN_COMMAND']='lmp'

atoms = Atoms("Si3O6", pbc=True, # alpha-quartz
      cell=[[4.91655781, 0.0, 0.0], [-2.458278904999999, 4.257863962634786, 0.0], [0.0, 0.0, 5.43162525]],
      scaled_positions=
       [[0.        , 0.46879823, 0.33333333],
       [0.53120177, 0.53120177, 1.        ],
       [0.46879823, 0.        , 0.66666666],
       [0.14453836, 0.73143979, 0.5478196 ],
       [0.26856021, 0.41309857, 0.21448627],
       [0.58690143, 0.85546164, 0.88115293],
       [0.73143979, 0.14453836, 0.4521804 ],
       [0.41309857, 0.26856021, 0.78551373],
       [0.85546164, 0.58690143, 0.11884707]])

def g_mol_mass(element):
  return atomic_masses[atomic_numbers[element]]/(units.kg/(1000.*units.mol))

def make_specorder(atoms):
  return list(set(atoms.symbols))

def make_table_pair_coeff(specorder, tablefn, pairs):
  res = []
  for (s1,s2),entryname in pairs.items():
    try:
      i1 = specorder.index(s1)+1
      i2 = specorder.index(s2)+1
      if i1 > i2:
        i1,i2 = i2,i1
      res.append(f'{i1} {i2} {tablefn} {entryname}')
    except ValueError:
      #print(f"WARNING: Skip {(s1,s2),entryname}", file=sys.stderr)
      pass
  n=len(specorder)
  assert len(res) == n*(n-1)/2 + n
  return res

def make_lammps_table_params(atoms, tablefn, pairs):
  so = make_specorder(atoms)# ['B', 'O']
  params = {}
  params['pair_style']='table spline 1000'
  params['pair_coeff']=make_table_pair_coeff(so, tablefn, pairs)
    
  params['mass'] = [f'{i+1} {g_mol_mass(e)}' for i,e in enumerate(so)]
  return so,params

def make_lammps_table(atoms, tablefn, pairs, port=None, unixsocket=None):

  so, params = make_lammps_table_params(atoms, tablefn, pairs)

  if port is not None:
    params['fix'] = [f'1 all ipi 127.0.0.1 {port}',]
    params['run'] = 100000
  elif unixsocket is not None:
    params['fix'] = [f'1 all ipi {unixsocket} 666 unix',]

  calc = LAMMPS(specorder=so, keep_tmp_files=True, parameters=params, files=[tablefn, ], label=f'lmp_', tmp_dir = f'tmp_lammps', verbose=True)
  
  return calc

def launch_lammps_ipi_client(args, atoms, properties, port, unixsocket):
  calc = make_lammps_table(atoms, args['tablefn'], args['pairs'], port, unixsocket)
  calc.atoms = atoms
  p = mp.Process(target=calc.run, args=(calc,))
  p.start()
  p.wait = p.join # Dirtiest hack for SocketIOCalculator.close
  print('launch_lammps_ipi_client')
  args['process'] = p
  return p
  

def make_lammps_calc_ipi(args, port=12347):
  socket_calc = SocketIOCalculator(launch_client=lambda atoms, properties, port, unixsocket:launch_lammps_ipi_client(args, atoms, properties, port, unixsocket), log='iPI.log', port=port)
  return socket_calc

def read_table_entry_pairs(table_entry_pairs_fn):
  with open(table_entry_pairs_fn, 'r') as table_entry_pairs_file:
    pairs = []
    for l in table_entry_pairs_file:
      l=l.strip()
      if len(l) == 0:
        continue
      try:
        s1, s2, entryname = l.split()
        pairs.append(((s1,s2),entryname))
      except ValueError:
        print (f"ValueError:({l})")
        raise
    pairs = dict(pairs)
  return pairs

infn = sys.argv[1]
outfn = sys.argv[2]
tablefn = sys.argv[3]
pairs = read_table_entry_pairs(sys.argv[4])
print ("pairs", pairs)


intrj = read(infn, index=':')
outtrj = TrajectoryWriter(outfn)

lammps_args = {'tablefn':tablefn, 'pairs':pairs}

prev_a = None
for ai,a in enumerate(intrj):
  t0 =time()
  a.pbc=True
  if (prev_a is None) or (len(a) != len(prev_a)) or (not (prev_a.numbers == a.numbers).all()):
    print ("New calc")
    if 'process' in lammps_args:
      print ("Joining previous process...")
      prev_a.calc.close()
      #lammps_args['process'].join()
      print ("Joined!")
    a.calc = make_lammps_calc_ipi(lammps_args)
  else:
    a.calc = prev_a.calc

  #s = a.get_stress()
  s = [0]
  e = a.get_potential_energy()
  f = a.get_forces()
  fmax = np.max(np.linalg.norm(f, axis=1))

  outtrj.write(a)
  print (ai, ' of ', len(intrj), f"{time() - t0:6.2f}", f'E = {e:10.4f} Fmax = {fmax:10.4f}\nStress = {list(s)}')
  prev_a = a


prev_a.calc.close()
