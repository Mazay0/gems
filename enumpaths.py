try:
  from susmost.acutils import make_ac_samples, extract_slab_unit_cell, load_ac
  from susmost.cell import universal_cell_size
except:
  print ("""SuSMoST Main package not found. It can be downloaded from here: 
https://susmost.com/downloads.html
Pick the latest version that matches yout Python version.
Example of the installation command: `pip install SuSMoST-1.1.post178+g64f0532-cp39-cp39-linux_x86_64.whl`
 """)
import sys, numpy as np, os
from glob import glob
from ase.io import read, write
import numpy as np, sys, itertools

def reorder_homogenous(s, f):
  weghted_permutations = [(np.linalg.norm(s.positions - f[p].positions), p) for p in itertools.permutations(range(len(f)))]
  sorted_permutations = sorted(weghted_permutations, key=lambda dp: dp[0])
  d_min,p_best = sorted_permutations[0]
  return f[p_best]

def reorder(s, f):
  nums = set(s.numbers)
  for n in nums:
    fn = reorder_homogenous(s[s.numbers==n], f[f.numbers==n])
    f.positions[f.numbers==n] = fn.positions
  return f

def clean_atoms(a):
  keys_to_remove = a.arrays.keys() - set(['numbers', 'positions', 'adsorption', 'basis', 'initial_magmoms'])
  for k in keys_to_remove:
    a.set_array(k, None)
  a.calc = None
  return a

def extract_uc_from_ac(fn):
  ac1 = load_ac(fn)
  euc = extract_slab_unit_cell(ac1.slab_atoms)
  return clean_atoms(euc)

if len(sys.argv) != 4: 
  print(f"""Usage: python3 {sys.argv[0]} <ads1.xyz> <ads2.xyz> <workdir>""")

fn1, fn2, workdir  = sys.argv[1:]

os.makedirs(workdir, exist_ok=True)

for i, fn in enumerate([fn1, fn2], start=1):
  a = read(fn)
  supercell_size = universal_cell_size(a.info.get('size', 1))
  print (f'{supercell_size=}')
  a = clean_atoms(a)
  write(f'{workdir}/ac{i}.xyz', [a], 'extxyz')

euc = extract_uc_from_ac(f'{workdir}/ac1.xyz')

write(f'{workdir}/empty-slab.xyz', [euc], 'extxyz')
r_cut_off = sum(np.linalg.norm(euc.cell[:2, :2], axis=1))
borders = tuple(['fixed'] + list(supercell_size[:2, :2].ravel()))
print (f'{borders=}')

make_ac_samples(f'{workdir}/samples', [f'{workdir}/empty-slab.xyz', f'{workdir}/ac1.xyz', f'{workdir}/ac2.xyz'],  r_cut_off = r_cut_off, borders=borders)

all_samples  = [read(fn) for fn in sorted(glob(f'{workdir}/samples/sample_???.xyz'))]
heterosamples = [a for a in all_samples if len(set([a.info['s1'], a.info['s2']])) == 2]

weighted_paths = []
for i,s in enumerate(heterosamples):
  a1 = s[s.arrays['adsorption'] == 'a1']
  a2 = s[s.arrays['adsorption'] == 'a2']
  slab = s[s.arrays['adsorption'] == 's']
  reorder(a1, a2)
  n1 = np.linalg.norm(a1.positions - a2.positions)
  a12 = a1 + a2
  frac_pos = (np.linalg.inv(euc.cell) @ a12.positions.T).T
  frac_shift = -np.floor(np.min(frac_pos, axis=0)) # to place ACs witin a cell
  shift = (euc.cell @ frac_shift.T).T
  a1.translate(shift)
  a2.translate(shift)
  slab.translate(shift)
  slab.wrap(eps=0.01)
  weighted_paths.append([n1, slab + a1, slab + a2])
  print(i, n1)

sorted_paths = sorted(weighted_paths, key=lambda x: x[0])
write(f'{workdir}/sorted_paths.xyz', sum([p[1:] for p in sorted_paths],[]), 'extxyz')
