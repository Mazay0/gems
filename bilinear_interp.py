import numpy as np
import matplotlib.pylab as plt

def hypint(a, x):
  # a + b*x + c*y +d*x*y = 0
  a,b,c,d = a
  if d == 0: # line
    return -(a*x + 0.5*b*x**2)/c
  l = np.log(np.abs(c + d * x)) # may be -inf
  print ("l=",l, ((b*c/(d**2)) -a/d)*l)
  return   - b*c/(d**2) - b*x/d + ((b*c/(d**2)) -a/d)*l

X = np.linspace(0., 1., 171)
Y = np.linspace(0., 1., 171)
#z_xy

#z00, z01, z10, z11 = -1., -1, 2, 2
#z00, z01, z10, z11 = -1., 0, 0, 1
z00, z01, z10, z11 = -1., 2, -1, 2

a = np.array([z00, z10 - z00, z01 - z00, z11 - z10 - z01 + z00])

print (a)

Z = X[None, :]*Y[:, None] * a[3] + (Y * a[2])[:, None] + (X * a[1])[None, :] + a[0]

if (a[3] == 0) and (a[2] == 0 or a[1] == 0):
  # z =k*x + b or z =k*y + b
  k = a[1] + a[2]
  b = a[0]
  x0 = -b / k
  S = min(max(0, x0), 1)
  if k > 0:
    S = 1.0 - S
  print (k,b, x0, S)

else:


  X0 = np.linspace(0., 1., 101)
  Y0 = -(a[0] + a[1]*X0) / (a[2] + a[3]*X0)

  xc = - a[2] / a[3] # hyperbola discontinuity point
  x0 = - a[0] / a[1] # y(x0) == 0
  x1 = - (a[0] + a[2])/ (a[1] + a[3]) # y(x1) == 1
  yc = - a[1] / a[3] # hyperbola horizontal asymptota
  #assert (x0 <= xc <= x1) or (x0 >= xc >= x1), f"Weird hyperbola: a={a}, x0={x0} xc={xc} x1={x1} yc={yc}"
  zc = a[0] - a[1]*a[2]/a[3] # Z value in hyperbola focus, also indicates direction of branches (if above or below zero)
  print ("zc:", zc)

  print (f"x0={x0} x1={x1}")
  print (f"xc={xc} yc={yc}")

  b0 = min(x0,x1,1.0) # b0 <= 1
  b0 = max(0, b0)     # b0 >= 0
  b1 = max(x0, x1,0)  # b1 >= 0
  b1 = min(1, b1)     # b1 <= 1
  assert(0 <= b0 <= b1 <= 1)

  print (f"b0={b0} b1={b1}")

  # areas of rectangles [0 - b0], [b0 - b1], [b1 - 1].
  ra1 = b0  - 0.0
  ra2 = b1  - b0
  ra3 = 1.0 - b1

  print ("areas of rectangles:", ra1, ra2, ra3)

  # May be inf's
  hi0 = hypint(a, 0.0)
  hib0 = hypint(a, b0)
  hib1 = hypint(a, b1)
  hi1 = hypint(a, 1.0)
  print ("hyperbola integrals", hi0, hib0, hib1, hi1)

  # areas under hyperbola branches
  ha1 = hib0 - hi0 if ra1 > 0.0 else 0.0
  ha2 = hib1 - hib0 if ra2 > 0.0 else 0.0
  ha3 = hi1 - hib1 if ra3 > 0.0 else 0.0

  print ("areas under hyperbola branches:", ha1, ha2, ha3)

  if xc < x0 and xc < x1:
    print("only right branch")
    S = ra2 - ha2 # ra2-ha2 - above right branch in rect2
    S += ra1 if x0 < x1 else ra3 # x0 < x1 means branch is growing, so ra3 is under branch
  elif xc > x0 and xc > x1:
    print("only left branch")
    S = ha2 # ha2 - under left branch in rect2
    S += ra3 if x0 < x1 else ra1 # x0 < x1 means branch is growing, so ra3 is under branch
  else:
    S = ha1 + (ra3 - ha3)
    if x0 > x1:
      S += ra2
    print("rigth and left branches")
   

  # by default look for areas in 3rd and 1st quadrants (under left branch and above right branch)
  if (a[3] < 0): # # areas in 2nd and 4th quadrants (above left branch and under right branch)
    S = 1.0 - S  

  plt.imshow(Z, origin='lower', extent=[0,1,0,1])
  plt.plot(X0,Y0,color='white')
  plt.plot([xc,xc], [0,1],color='red')
  plt.plot([0,1], [yc,yc],color='red')
  plt.plot([x0,x0], [0,1],color='black')
  plt.plot([x1,x1], [0,1],color='blue')
  plt.xlim(0,1)
  plt.ylim(0,1)
  plt.colorbar()
  plt.show()


Snumeric = np.sum(Z >= 0) / np.product(Z.shape)
discr = S - Snumeric
print ("S =",S, f"{Snumeric=}", "discr=",discr)

assert(abs(discr) < 0.01)
