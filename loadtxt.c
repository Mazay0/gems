#include <stdio.h>
#include <assert.h>
#include <complex.h>

void loadtxtz(FILE *f, int N, double complex *data)
{
  double re, im;
  for(int i = 0; i < N; ++i)
  {
    int res = fscanf(f, " (%lf %lfj) ",&re, &im);
    data[i] = re + I*im;
  }
}

int main(int argc, char *argv[])
{
  assert(argc == 2);

  FILE *f = fopen(argv[1],"r");
  int N = 3;
  double complex data[N][N];
  
  loadtxtz(f, N*N, &data[0][0]);
  for(int i = 0; i < N; ++i)
  for(int j = 0; j < N; ++j)
  {
    printf("%f %f\n", creal(data[i][j]), cimag(data[i][j]));
  }
  fclose(f);
}
