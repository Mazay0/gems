#
#
# Why Neural Networks can learn (almost) anything:  https://www.youtube.com/watch?v=0QczhVg5HaI
#
#


import numpy as np
import torch
from torch import nn
import matplotlib.pylab as plt


device = (
    "cuda"
    if torch.cuda.is_available()
    else "mps"
    if torch.backends.mps.is_available()
    else "cpu"
)
device='cpu'
print(f"Using {device} device")


class MyNN(nn.Module):
    def __init__(self):
        super().__init__()
        self.linear_relu_stack = nn.Sequential(
            nn.Linear(1, 3).to(device),
            nn.LeakyReLU().to(device),
            nn.Linear(3, 1, bias=False).to(device)
        )

    def forward(self, x):
        logits = self.linear_relu_stack(x)
        return logits

def ground_truth(x):
  return (x+10.)*(x+7.)*(x-1.5)*(x-9)*0.01

x = np.linspace(-10, 9, 30)
y = ground_truth(x)

loss_fn = nn.MSELoss()

mynn =MyNN()

mynn.train(True)
optimizer = torch.optim.Adam(mynn.parameters(), lr=0.1)

xt = torch.Tensor(x.reshape((-1, 1))).to(device)
yt = torch.Tensor(y.reshape((-1, 1))).to(device)


for i in range(5001):
  y_pred = mynn(xt)
  loss = loss_fn(yt, y_pred)
  optimizer.zero_grad()
  loss.backward()
  optimizer.step()
  if i%100 == 0:
    y_pred = mynn(xt)
    loss = loss_fn(yt, y_pred)
    print("Loss 100: ", loss.item())


z = mynn(xt).cpu().detach().numpy()

plt.plot(x,y, '-o', c='k', label='ground truth')
plt.plot(x,z, ':x', c='red', label='approximation')
plt.legend()
plt.show()







