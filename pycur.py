from ctypes import *
from time import sleep

nc = cdll.LoadLibrary('libncursesw.so.6')
nc.initscr.restype = c_void_p
nc.mvaddch.argtypes = [c_int, c_int, c_wchar]
nc.mvaddstr.argtypes = [c_int, c_int, c_char_p]
lines = c_int.in_dll(nc, "LINES")
cols = c_int.in_dll(nc, "COLS")


win = nc.initscr()

print (lines.value, cols.value)

res=nc.cbreak()
print(res)
res = nc.noecho()
print(res)

res = nc.clear()
print(res)

nc.mvaddch(10, 10, c_wchar('+'))

nc.mvaddstr(30, 10, 'Å привет'.encode('utf-8'))

res = nc.refresh()

ch = nc.getch()

nc.endwin()

print (res)
print (ch)

