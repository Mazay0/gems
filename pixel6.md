# Fix boot on low charge: avoiding infinite boot - power off loop

https://www.reddit.com/r/GooglePixel/comments/19f202u/pixel_6_pro_turning_on_automatically_when/

I have a Google Pixel 6a, with this same exact issue:

How to fix the issue at hand:
Assuming the phone is in an active power boot loop, as its coming online boot into your FastBoot Menu (Power Button + Volume Down Button), and let it sit for a few minutes to charge. The Fastboot menu doesn't take nearly as much power as when it fully boots into the OS.

I will reply with how to turn this off, and have it not automatically turn on when plugged in if I figure out how, but for now, doing the above is how you can charge your battery while stuck in a power boot loop

(I'm not entirely sure, but you might have to restart the phone with the restart button in order to use the buttons to get to the fastboot menu, if that's the case then you have to wait for the phone to boot up, press the power button + up volume button, and click restart and then do the power button + volume down button. When my phone was stuck I booted it multiple times with and without me hitting restart so idk if that's a requirement to get into fastboot or not. None the less, if you get into the fastboot menu, it'll work)

Potential Permanent Fix (I've seen this work for some, and not for others, but it's worth saying so you can take a shot at it):

USB Debugging must be enabled for this to work, so if you do not have it enabled and are currently stuck in a power boot loop, look at the message above this one to get out of the loop initially, and then do the following to turn off the auto power on

To enable USB Debugging, go to Settings on your phone

Search for "USB Debugging"

Turn it on

If you don't see an option to turn USB Debugging on, you may not be a developer. So, to enable Developer Options:

Go to Settings>About Phone

Scroll down and tap the Build Number over and over until it tells you that you are a developer

Then proceed to turn on USB debugging as mentioned above

-----------

Download Android SDK Tools:

https://dl.google.com/android/android-sdk_r24.4.1-windows.zip?utm_source=androiddevtools&utm_medium=website

Extract everything and run SDK Manager.exe (If it doesn't launch, make sure you have Java installed):

https://www.java.com/download/ie_manual.jsp

Select/Check "Android SDK Platform-tools" and "Android SDK Build-tools"

They should be automatically selected anyways, but if they aren't then that's what you want to select

Click Install All Packages

In the popup window, click on each package and accept the License, then click Install

Go back to the main folder, and go to the new folder "platform-tools"

Shift + Right Click in any blank space and open up a Command Prompt

Restart the phone

Boot into FastBoot Mode

Type the following command into the Command Prompt (Without Quotations):

"fastboot oem off-mode-charge 1"

Press Enter

Plug USB into PC while in FastBoot Mode

--------------


If it just says "Waiting for device" WITH it being plugged in, do the following:

Download this USB Driver:

https://drive.google.com/file/d/16mrVQ2dErcmPR8_z_jKqR4Toabnw4Thl/view?pli=1

Extract all of it, and go to AMD or Intel based on your build

Right click and install android_winusb.inf

Then go back and try to run the command again, if it still says the same thing, download Microsoft Visual C++ 2015:

https://www.microsoft.com/en-us/download/details.aspx?id=52685

Then try again. If it doesn't work again, search Google, as I no longer know why and can't help

Then type (Without Quotations):

"fastboot reboot"

Press Enter

Wait for the phone to boot

Once it boots, unplug the USB Cable

Then Shutdown the phone

Now when you plug the device in when it's dead, or turned off, it should not automatically turn back on

Again, this may not work for all, I've seen videos of it working, but it hasn't personally helped me. But I figured it's better to post this solution on the off chance it helps someone

Thank you for reading! And good luck!

