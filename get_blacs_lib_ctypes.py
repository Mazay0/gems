from ctypes import cdll, c_double, c_int, cast, py_object, POINTER, c_void_p, c_char_p, Structure, CFUNCTYPE, byref

l1 = cdll.LoadLibrary("/home/mazay/prg/aims/build-so-3/libaims.220225.scalapack.mpi.so") # any library with BLACS dependency

def get_blacs_lib():
  libdl = cdll.LoadLibrary('libdl.so.2')
  libdl.dlopen.restype = c_void_p
  libdl.dlerror.restype = c_char_p
  libdl.dlclose.restype = c_int
  libdl.dl_iterate_phdr.restype = c_int

  class dl_phdr_info(Structure):
    _fields_ = [("dlpi_addr", c_void_p),
                ("dlpi_name", c_char_p)]

  dl_phdr_callback = CFUNCTYPE(c_int, POINTER(dl_phdr_info), c_int, c_void_p)
  libdl.dl_iterate_phdr.argtypes = [dl_phdr_callback, c_void_p]

  def dl_phdr_callback_impl(dl_phdr_info_ptr, size, data):
    libname = dl_phdr_info_ptr.contents.dlpi_name.decode("utf-8")
    lib = cdll.LoadLibrary(libname)
    if hasattr(lib,"blacs_pinfo_"):
      result_list = cast(data, py_object).value
      result_list += [libname]
      lib.blacs_pinfo_.restype = None
      lib.blacs_pinfo_.argtypes = [POINTER(c_int), POINTER(c_int)]
      mypnum = c_int()
      nprocs = c_int()
      lib.blacs_pinfo_(byref(mypnum), byref(nprocs))
      print (f"mypnum = {mypnum} nprocs = {nprocs} {lib}")
    return 0

  dl_phdr_callback_inst = dl_phdr_callback(dl_phdr_callback_impl)
  res = []
  x = libdl.dl_iterate_phdr(dl_phdr_callback_inst, c_void_p.from_buffer(py_object(res)))
  print (f"x={x}")
  print (res)


print (get_blacs_lib())
