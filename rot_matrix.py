def rot_matrix(deg, plane=None):
  theta = np.radians(deg)
  c, s = np.cos(theta), np.sin(theta)
  R = np.array(((c, -s), (s, c)))
  if plane is None:
    return R
  i = {'yz':0, 'xz':1, 'xy':2}[''.join(sorted(plane)).lower()]
  R = np.insert(R, i, [0, 0], axis=0)
  R = np.insert(R, i, [0, 0, 0], axis=1)
  return R
