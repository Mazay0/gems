import sys, csv
import numpy as np
import urllib.request
from scipy.optimize import minimize

from ase import Atoms
from ase.calculators.emt import EMT
from ase.calculators.eam import EAM
from ase.build import bulk, make_supercell
from ase.optimize.lbfgs import LBFGSLineSearch, LBFGS
from ase.constraints import ExpCellFilter, StrainFilter
from ase.stress import voigt_6_to_full_3x3_strain, full_3x3_to_voigt_6_strain, full_3x3_to_voigt_6_stress
from ase.units import GPa
from ase.parallel import parprint, world
from ase.io import read
from ase.io.trajectory import TrajectoryWriter
from gpaw import GPAW, PW

if world.rank==0:
  eam_fn, _ = urllib.request.urlretrieve("https://www.ctcms.nist.gov/potentials/Download/2001--Mishin-Y-Mehl-M-J-Papaconstantopoulos-D-A-et-al--Cu-1/2/Cu01.eam.alloy")

  csv_fn, _ = urllib.request.urlretrieve("https://www.ctcms.nist.gov/potentials/entry/2001--Mishin-Y-Mehl-M-J-Papaconstantopoulos-D-A-et-al--Cu-1/2001--Mishin-Y--Cu-1--LAMMPS--ipr1/elastic.Cu.csv")

  with open(csv_fn,'r') as rf:
    r = csv.reader(rf, delimiter=',')
    headers = next(r)
    row = next(r)
    ethalon_C = np.array(row[16:]).astype(float).reshape((6,6))
    np.savetxt(sys.stdout, ethalon_C, fmt="%10.2f")


def numeric_stress(atoms, d=1e-6, voigt=True, force_consistent=True):
  '''
    Taken from `ase/calculators/test.py`, but added `force_consistent` argument
  '''
  stress = np.zeros((3, 3), dtype=float)

  cell = atoms.cell.copy()
  V = atoms.get_volume()
  for i in range(3):
      x = np.eye(3)
      x[i, i] += d
      atoms.set_cell(np.dot(cell, x), scale_atoms=True)
      eplus = atoms.get_potential_energy(force_consistent=force_consistent)

      x[i, i] -= 2 * d
      atoms.set_cell(np.dot(cell, x), scale_atoms=True)
      eminus = atoms.get_potential_energy(force_consistent=force_consistent)

      stress[i, i] = (eplus - eminus) / (2 * d * V)
      x[i, i] += d

      j = i - 2
      x[i, j] = d
      x[j, i] = d
      atoms.set_cell(np.dot(cell, x), scale_atoms=True)
      eplus = atoms.get_potential_energy(force_consistent=force_consistent)

      x[i, j] = -d
      x[j, i] = -d
      atoms.set_cell(np.dot(cell, x), scale_atoms=True)
      eminus = atoms.get_potential_energy(force_consistent=force_consistent)

      stress[i, j] = (eplus - eminus) / (4 * d * V)
      stress[j, i] = stress[i, j]
  atoms.set_cell(cell, scale_atoms=True)

  if voigt:
      return stress.flat[[0, 4, 8, 5, 2, 1]]
  else:
      return stress

def optimize_cell_numeric(atoms, trajfn=None, minimize_disp=False, numeric_d=1e-10, force_consistent=None):
  if force_consistent is None:
    force_consistent = "free_energy" in atoms.calc.implemented_properties

  c0 = np.array(atoms.cell)
  bounds = np.eye(3).ravel()[:,None] + np.array([[ -0.5, 0.5 ]] * 9)

  def func(x):
    atoms.set_cell(c0 @ x.reshape((3,3)), scale_atoms=True)
    E = atoms.get_potential_energy()
    atoms.set_cell(c0, scale_atoms=True)
    return E

  def jac(x):
    atoms.set_cell(c0 @ x.reshape((3,3)), scale_atoms=True)
    S = numeric_stress(atoms, d=numeric_d, voigt=False, force_consistent=force_consistent)
    E = atoms.get_potential_energy()
    atoms.set_cell(c0, scale_atoms=True)
    return S.ravel()*atoms.get_volume()

  x0 = np.eye(3)
  opt = minimize(func, x0.ravel(), method='SLSQP', jac=jac, bounds=bounds, options={'disp': minimize_disp}, tol=1e-15)
  atoms.set_cell(c0 @ opt.x.reshape((3,3)), scale_atoms=True)
  return opt.x

def apply_strains(atoms, strains):
  res = []
  for s in strains:
    atoms2 = atoms.copy()
    atoms2.set_cell(atoms.cell @ s, scale_atoms=True)
    res.append(atoms2)
  return res

def calc_stresses(atoms_list, calc=None, numeric_d=1e-5, numeric_force_consistent=None):
  stresses = []
  Eextrap = []
  Efree = []
  for atoms in atoms_list:
    if calc is not None:
      atoms.calc = calc
    
    LBFGS(atoms).run(fmax=0.01)
    Eextrap.append(atoms.get_potential_energy(force_consistent=False))
    Efree.append(atoms.get_potential_energy(force_consistent=True))

    if "stress" in atoms.calc.implemented_properties:
      stress = atoms.get_stress(voigt=False)
    else:
      stress = numeric_stress(atoms, d=numeric_d, voigt=False, 
                              force_consistent=("free_energy" in atoms.calc.implemented_properties) if numeric_force_consistent is None else numeric_force_consistent)
    stresses.append(stress)
  return stresses, np.array(Eextrap), np.array(Efree)

# strains from https://doi.org/10.1016/j.cpc.2009.11.017
ULICStrains = np.array([[1, 2, 3, 4, 5, 6],
[-2, 1, 4, -3, 6, -5],
[3, -5, -1, 6, 2, -4],
[-4, -6, 5, 1, -3, 2],
[5, 4, 6, -2, -1, -3],
[-6, 3, -2, 5, -4, 1]])
'''
Table 3 from https://doi.org/10.1016/j.cpc.2009.11.017
+----------------+--------------------------+
| Crystal system | Number of ULICS required |
+================+==========================+
| Cubic          | 1                        |
| Hexagonal      | 2                        |
| Rhombohedral   | 2                        |
| Tetragonal     | 2                        |
| Orthorhombic   | 3                        |
| Monoclinic     | 5                        |
| Triclinic      | 6                        |
+----------------+--------------------------+
'''

def atoms2Cij(a):
  voigt_strains = np.vstack( [-np.eye(6)*0.005, np.eye(6)*0.005, -np.eye(6)*0.01, np.eye(6)*0.01])
  GL_strains = [voigt_6_to_full_3x3_strain(s)  for s in voigt_strains]  # Green-Lagrange strains
  phys_strains = [s - np.eye(3) for s in GL_strains] # physical strains; \epsilon in 10.1016/j.cpc.2013.03.010 formulas (1)-(3).
  L_strains = [s + 0.5 * (s * s) for s in phys_strains] # Lagrangian strain, \eta in 10.1016/j.cpc.2013.03.010 formulas (1), (4).
  traj = apply_strains(a, GL_strains)
  if world.rank==0:
    print("cells:")
    for i,ai in enumerate(traj):
      print('cell',i)
      np.savetxt(sys.stdout, np.array(ai.cell), fmt="%10.6f")
      print('L_strain',i)
      np.savetxt(sys.stdout, L_strains[i], fmt="%10.6f")

  stresses, Eextrap, Efree = calc_stresses(traj, calc=a.calc)
  voigt_stresses = np.array([full_3x3_to_voigt_6_stress(s)  for s in stresses])
  if world.rank==0:
    print("voigt_stresses")
    np.savetxt(sys.stdout, voigt_stresses, fmt="%10.6f")
    print("Eextrap", Eextrap)
    print("Efree", Efree)
  return voigt_stresses.T @ np.linalg.pinv(voigt_strains.T) / GPa

def traj2strains(traj, eq_cell):
  eq_cell_inv = np.linalg.inv(eq_cell)
  GL_strains = [eq_cell_inv @ a.get_cell() for a in traj]
  voigt_strains = np.array([full_3x3_to_voigt_6_strain(s) for s in GL_strains])
  return voigt_strains
  
def traj2stresses(traj):
  return np.array([a.get_stress(voigt=True)  for a in traj])

def traj2Cij(traj, eq_cell):
  voigt_strains = traj2strains(traj, eq_cell)
  voigt_stresses = traj2stresses(traj)
  return voigt_stresses.T @ np.linalg.pinv(voigt_strains.T) / GPa



#a = bulk('Cu', cubic=True) * (2,1,1)
#a = bulk('Cu')
#a = make_supercell(a, [[-1, 1, 1],[1,-1,1],[1,1,-1]] )
#a.calc = EMT()
#a.calc = EAM(potential="Cu01.eam.alloy")
a = Atoms("Si3O6", pbc=True, # alpha-quartz
      cell=[[4.91655781, 0.0, 0.0], [-2.458278904999999, 4.257863962634786, 0.0], [0.0, 0.0, 5.43162525]],
      scaled_positions=
       [[0.        , 0.46879823, 0.33333333],
       [0.53120177, 0.53120177, 1.        ],
       [0.46879823, 0.        , 0.66666666],
       [0.14453836, 0.73143979, 0.5478196 ],
       [0.26856021, 0.41309857, 0.21448627],
       [0.58690143, 0.85546164, 0.88115293],
       [0.73143979, 0.14453836, 0.4521804 ],
       [0.41309857, 0.26856021, 0.78551373],
       [0.85546164, 0.58690143, 0.11884707]])


a.calc = GPAW(xc='PBE', mode=PW(500), txt="gpaw.log", # by 10.1016/j.commatsci.2017.06.015 PBE and modest PW basis are sufficent for good accuracy
              kpts=(2,2,2),                           # by 10.1016/j.commatsci.2017.06.015 huge k-grid may be needed
              symmetry='off',                         # !!! CRUCIAL !!!
              occupations={'name': 'fermi-dirac', 'width': 0.1}, # fermi-dirac gives large force inconsistency and smearing error
              #occupations={'name': 'marzari-vanderbilt', 'width': 0.01},  # marzari-vanderbilt gives Zero force inconsistency and small smearing error; but may give negative occupancies
              convergence={'energy': 0.000001,  # eV / electron
                           'density': 1.0e-4,  # electrons / electron
                           'eigenstates': 4.0e-8,  # eV^2 / electron
                           'bands': 'occupied'},
              parallel={'sl_auto':True, 'use_elpa':True})

parprint ("E Extrapolated (force_consistent=False, default):", a.get_potential_energy(force_consistent=False))
parprint ("E Free energy  (force_consistent=True )         :", a.get_potential_energy(force_consistent=True))
parprint ("E <Free energy> - <Extrapolated> (force inconsistency):", a.get_potential_energy(force_consistent=True) - a.get_potential_energy(force_consistent=False))


if "stress" in a.calc.implemented_properties:
  '''
    * You can't get final optimization error below force inconsistency
      Force inconsistency depends from smearing (occupations option)
    
    * But low smearing cause poor elastic constants. Probably due to poor numeric differentiation.
    
    * Also low smearing hinders geometry optimization convergence
    
    * High smearing is a cheap alternative to large k-grid (kinda mean-field approximation?)
    
    * fmax must be consistent with SCF loop accuracy
    
    * LBFGS aims force to be below fmax. But energy may go uphill, even force consistent energy, 
      but especially extrapolated energy.
    
    * LBFGSLineSearch guarantees to return the lowest energy found, but linear search may
      fail if force is inconsistent with energy. It may happen even with force_consistent=True
      due to SCF inaccuracy or something else.
  '''
  LBFGS(StrainFilter(a), trajectory='rlxC.traj', force_consistent=True).run(fmax=0.1)
  LBFGS(a, trajectory='rlxP.traj', force_consistent=True).run(fmax=0.01)
  LBFGS(StrainFilter(a), trajectory='rlxC.traj', force_consistent=True).run(fmax=0.0001)
else:
  optimize_cell_numeric(a, )
parprint ("E Extrapolated (force_consistent=False, default):", a.get_potential_energy(force_consistent=False))
parprint ("E Free energy  (force_consistent=True )         :", a.get_potential_energy(force_consistent=True))
parprint(a.cell.cellpar(), a.cell.volume)
parprint ('Stress:', a.get_stress(voigt=True))


C = atoms2Cij(a)

if world.rank==0:
  np.savetxt(sys.stdout, C, fmt="%10.2f")
  print("RSE:", np.linalg.norm(C - ethalon_C))
  

parprint(
'''
+--------------------+-----------+----------+---------+-----------+----------+---------+-----------+----------+-------------------+
|                    | 4x4x4                          | 6x6x6                          | 10x10x10                                 |
+====================+===========+==========+=========+===========+==========+=========+===========+==========+===================+
| Fermi-Dirac  Width | RSE  C_ij | E_extrap | E_free  | RSE  C_ij | E_extrap | E_free  | RSE  C_ij | E_extrap | E_free            |
| 0.01               | 130.8709  | -3.6424  | -3.6424 | 482.7374  | -3.6538  |         | 60.2910   | -3.6690  | -3.66909207745567 |
| 0.02               |           |          |         | 309.2478  | -3.6545  |         |           |          |                   |
| 0.05               |           |          |         | 135.0435  | -3.6567  |         |           |          |                   |
| 0.1                | 165.1003  | -3.6184  | -3.6318 | 78.1877   | -3.6598  | -3.6673 | 52.9933   | -3.6687  | -3.67298220399721 |
| 0.2                |           |          |         | 58.6264   | -3.6629  |         |           |          |                   |
| 0.3                | 97.6116   | -3.6347  | -3.7043 | 50.3832   | -3.6616  |         | 19.0367   | -3.6621  | -3.71370486135144 |
| 0.5                | 89.9540   | -3.6238  | -3.8338 | 39.3229   | -3.6358  | -3.8276 |           |          |                   |
|                    |           |          |         |           |          |         |           |          |                   |
+--------------------+-----------+----------+---------+-----------+----------+---------+-----------+----------+-------------------+
''')
