import sys
from ase.io import read, write
from ase.cell import Cell
import numpy as np
from susmost.acutils import load_ac, get_unit_cell, get_adsorbate, get_slab
from susmost.cell import make_int_supercell
from itertools import product
from pathlib import Path

fn = sys.argv[1]
atoms = read(fn)

slab_center = atoms.cell.cartesian_positions([0.5,0.5,0.0])


(uc, sc), ads, slab = get_unit_cell(atoms), get_adsorbate(atoms), get_slab(atoms)
uc = Cell(uc)
mass_center_orig = np.mean(ads.positions, axis=0)
mass_center_frac = uc.scaled_positions(mass_center_orig)
mass_center_frac %= 1.0
mass_center_frac[2] = 0.0
mass_center = uc.cartesian_positions(mass_center_frac)


sc_coords  = [s for s in make_int_supercell(sc)]
dists = [np.linalg.norm(uc.cartesian_positions(s) + mass_center - slab_center) for s in sc_coords]
sc_idx = np.argmin(dists)
s = sc_coords[sc_idx]

shift = mass_center - mass_center_orig + uc.cartesian_positions(s)
shift[2] = 0

atoms.translate(shift)
atoms.wrap()

write(f'cent_{Path(fn).stem}.xyz', [atoms],'extxyz')


