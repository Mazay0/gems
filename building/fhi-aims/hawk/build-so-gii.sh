#!/bin/sh

module purge
module load cmake
module load python/3.10.4-sql
module load mpi/intel
module load mkl
module load compiler/gnu/12/1.0
module unload compiler/gnu/9/2.0
module list

echo $MPI_ROOT

export AIMS_HOME=`realpath $HOME/installs/aims-so-gii/`
export BUILD_DIR=${PWD}/build-so-gii

rm -rf $BUILD_DIR
mkdir $BUILD_DIR
cd $BUILD_DIR

cmake --warn-uninitialized -DCMAKE_Fortran_COMPILER=mpif90 -B $BUILD_DIR -C ../cmake/toolchains/libaims.gnu.cmake .. | tee cmake.log

make -j 64  | tee make.log
make install | tee make_install.log

