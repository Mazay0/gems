import numpy as np
import re, json, sys


if len(sys.argv) != 3:
	print (f"Usage: {sys.argv[0]} <job.txt> <coverage.json>")

with open(sys.argv[1],'r') as jobf:
	job_lines = jobf.readlines()

times = []
for l in job_lines:
	m = re.search("^real\s+([0-9]+)m([0-9\.]+)s$", l)
	if m is not None:
		try:
			t = int(m[1])*60 + float(m[2])
			times.append(t)
		except:
			print ("Error:", l)

time_cmd_lines = [l for l in job_lines if '$ time ' in l]
assert len(time_cmd_lines) == len(times), f"{len(time_cmd_lines), len(times)}"

context2idx = {}
context_times = []
idx2context = []

for i,l in enumerate(time_cmd_lines):
	m = re.search(" \$COVER_CMD \-\-context\=([\w\-]+) ", l)
	if m is not None:
		ctx = m[1]
		assert ctx not in context2idx
		
		idx = len(idx2context)
		idx2context.append(ctx)
		
		context2idx[ctx] = idx
		context_times.append(times[i])

N = len(context2idx)
assert N == len(context2idx)
assert N == len(context_times)
assert N == len(idx2context)

print (context_times, sum(context_times) / 60.0)

cov = json.load(open(sys.argv[2],'r'))
lines = []
for fn,file_data in cov['files'].items():
	lines += [(fn, int(l)) for l in file_data['executed_lines']] + [(fn, int(l)) for l in file_data['missing_lines']]

lines = sorted(lines)
M = len(lines)
lines2idx = {l:idx for idx,l in enumerate(lines)}
assert(M == len(lines2idx))
print (len(lines), "=?=", cov['totals']['covered_lines'] +cov['totals']['missing_lines'], cov['totals']['covered_lines'])
print (context2idx)
A = np.zeros((M, N))
for fn,file_data in cov['files'].items():
	for l, contexts in file_data['contexts'].items():
		try:
			line_idx = lines2idx[(fn, int(l))]
		except KeyError as err:
			continue
		for context in contexts:
			context_idx = context2idx[context]
			A[line_idx, context_idx] = 1


import cvxpy as cp

w = np.array(context_times)
x = cp.Variable(N, boolean=True)
objective = cp.Maximize(cp.sum( cp.minimum(1, A @ x)))
constraints = [cp.sum(cp.multiply(x, w)) <= 400.0, ]

prob = cp.Problem(objective, constraints)
result = prob.solve()

for idx, ctx in enumerate(context2idx):
	print (f'{idx:10} {ctx:30} {int(x.value[idx]): 5} {context_times[idx]/60.0:10.2}')

cov0 = np.minimum(1, np.sum(A, axis=1))
covX = np.minimum(1, A @ x.value)
print (sum(cov0))
print (sum(covX))

for i,(x0,x1) in enumerate(zip(cov0, covX)):
	if x0 != x1:
		fn, lidx = lines[i]
		print(fn, lidx,  cov['files'][fn]['contexts'][str(lidx)])





