from ase.io import read
import sys, numpy as np
from csv import DictWriter


data = []
fields = set()
fields_dict = dict.fromkeys(['formula', 'Ep', 'Ek', 'fmax', 'mass', 'a','b','c', 'A', 'B', 'C', 'vol'])
for infn in sys.argv[1:]:
  traj = read(infn)

  for atoms in traj:
    info = dict(atoms.info)
    info['formula'] = atoms.get_chemical_formula()
    info['mass'] = sum(atoms.get_masses())
    info['Ep'] = atoms.get_potential_energy()
    info['Ek'] = atoms.get_kinetic_energy()
    f = atoms.get_forces()
    info['fmax'] = max(np.linalg.norm(f, axis=-1))
    try:
      info['vol'] = atoms.get_volume()
      for k,v in zip("abcABC",atoms.cell.cellpar()):
        info[k] = v
    except ValueError:
      pass
    data.append(info)
    fields_dict.update(dict.fromkeys(info.keys()))

#print(fields_dict.keys())

csv = DictWriter(sys.stdout, fieldnames=fields_dict.keys())
csv.writeheader()
for d in data:
  csv.writerow(d)

