import numpy as np, sys
import matplotlib.pylab as plt
from scipy.integrate import quad
import numba as nb
from time import time

@nb.njit("float64(float64)")
def gauss_int(stdev):
  return (stdev*np.sqrt(2.0*np.pi))


@nb.njit
def gauss(x=0.0, mean=0.0, stdev=1.0, A=None):
  if A is None:
    A=1.0/gauss_int(stdev)
  return A*np.exp(-0.5*(((x-mean)/stdev)**2))

def gauss_prod(mean1, stdev1, A1, mean2, stdev2, A2):
  D1 = stdev1**2
  D2 = stdev2**2
  D12 = D1 + D2
  mean = (mean1 * D2 + mean2 * D1)/D12
  D = (D1*D2/D12)
  stdev = D**0.5
  A = gauss(mean, mean1, stdev1, A1) * gauss(mean, mean2, stdev2, A2) / gauss(mean, mean, stdev, 1.0)
  return mean, stdev, A

def gauss_contr(mean1, stdev1, A1, mean2, stdev2, A2):
  mean, stdev, A = gauss_prod(mean1, stdev1, A1, mean2, stdev2, A2)
  return A * gauss_int(stdev)

#@nb.njit
def dual_basis(x, basis, weights):
  B = np.array([fj(x) for fj in basis])
  return B.T @ weights


grid = np.arange(-10, 10.+1e-6, 1.0)
X = np.linspace(-15,15,3000)

# Basis
f = [lambda t, x0=x0: gauss(t, x0, 1) for x0 in grid]
# Overlap matrix of the basis
S = np.array([[gauss_contr(m1, 1.0, 1.0/gauss_int(1.0), m2, 1.0, 1.0/gauss_int(1.0)) for m2 in grid] for m1 in grid])
print ("Eivals:",np.linalg.eigh(S)[0])
# Overlap matrix of the dual basis

#Sinv = np.linalg.inv(S) # poor behaviour if S is singular
Sinv = np.linalg.pinv(S) # stable even when S is singular

# Dual basis
phi = [lambda t, Sinv_i=Sinv_i:  dual_basis(t, f, Sinv_i) for Sinv_i in Sinv]

'''
for fi in f:
  plt.plot(X, fi(X),c='b')
plt.show()
'''
'''
plt.imshow(S)
plt.show()
plt.imshow(Sinv)
plt.show()
'''
'''
for i,phi_i in enumerate(phi):
  if (i % 5 != 0):
    continue
  a = (i+1) / len(phi)
  plt.plot(X, phi_i(X), c=(a, 1-abs(a-0.5)*2 , 1-a))
plt.show()
'''

func1 = nb.njit(lambda x:gauss(x, -5.5, 1.0, 1) + gauss(x, 5, 1.0, 1))
#func1 = nb.njit(lambda x:np.sin(x*1))

c1 = np.array([quad(lambda x: fi(x)*func1(x) , -15, 15, limit=500)[0] for fi in f]).T @ Sinv
print(c1)
plt.plot(X, func1(X), c='k')
plt.plot(grid, np.abs(c1)/np.max(np.abs(c1)), '.',c='g')
plt.plot(X, [sum([fj(xi) * c1j for fj,c1j in zip(f, c1)]) for xi in X], '--', c='b')

s1 = np.array([quad(lambda x: fi(x)*func1(x) , -15, 15, limit=500)[0] for fi in phi]).T @ S
print(s1)
plt.plot(grid, np.abs(s1)/np.max(np.abs(s1)), '+',c='g')
print(np.linalg.norm(S @ c1 - s1))
plt.plot(X, [sum([fj(xi) * c1j for fj,c1j in zip(phi, s1)]) for xi in X], '-.', c='r')
plt.show()

sys.exit()
# Overlap matrix of the basis
t0 = time()
S = np.array([[quad(lambda x, phi_i=phi_i, phi_j=phi_j: phi_i(x)*phi_j(x), -15, 15)[0] for phi_j in phi] for phi_i in phi])
print (time() - t0)
plt.imshow(S)
plt.show()

