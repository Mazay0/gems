#!/usr/bin/env python3


import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import sys
from csv import reader
import argparse
import re

col_re = r"(d|a|l|ad|ld|)?([0-9]+)"

def parse_cols(arg_cols):
  cols = []
  funcs = []
  for c in arg_cols:
    m = re.match(col_re, c)
    if m[1] == '':
      func = lambda x: x
    elif m[1] == 'd':
      func = lambda x: x[1:] - x[:-1]
    elif m[1] == 'a':
      func = lambda x: np.abs(x)
    elif m[1] == 'l':
      func = lambda x: np.log10(np.abs(x))
    elif m[1] == 'ad':
      func = lambda x: np.abs(x[1:] - x[:-1])
    elif m[1] == 'ld':
      func = lambda x: np.log10(np.abs(x[1:] - x[:-1]))
    else:
      raise Exception(f'Unknown function: {m[1]}')
    col = int(m[2])
    cols.append(col)
    funcs.append(func)
  return cols, funcs

parser = argparse.ArgumentParser(description='Plot anything')
parser.add_argument('cols', metavar='N', nargs='+', help=f'Column numbers in input data: {col_re}')

args = parser.parse_args()

cols,funcs = parse_cols(args.cols)

df = pd.read_table(sys.stdin, sep='\s+', header=None, engine='c')
X = df[cols].to_numpy()
print (X)
fig = plt.figure()
ax1 = fig.add_axes([0.1, 0.1, 0.9, 0.9]) # main axes

for i in range(X.shape[1]):
  ax1.plot(funcs[i](X[:,i]))
ax1.grid(True)
plt.show()




