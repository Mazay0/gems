import ase, gpaw, numpy as np
from ase.optimize import LBFGSLineSearch
from ase.vibrations import Vibrations
from ase.thermochemistry import ThermoChem, IdealGasThermo

def get_geometry_symmetrynumber(atoms: ase.Atoms) -> (str, int):
  '''
  Table 10.1 Rotational symmetry numbers for molecular point groups from
  Essentials of Computational ChemistryTheories and Models, Second Edition, Christopher J. Cramer
  http://elibrary.bsu.az/books_400/N_176.pdf
  http://elibrary.bsu.edu.az/files/books_400/N_176.pdf
  '''
  from pymatgen.io.ase import AseAtomsAdaptor
  from pymatgen.symmetry.analyzer import PointGroupAnalyzer

  pga = PointGroupAnalyzer(AseAtomsAdaptor.get_molecule(atoms))
  sch = pga.sch_symbol
  symmetrynumber = sum([np.linalg.det(s.rotation_matrix) > 0 for s in pga.get_symmetry_operations()])
  if len(atoms) == 1:
    geometry = 'monoatomic'
    assert sch == 'Kh'
  elif sch in ['C*v', 'D*h']:
    geometry = 'linear'
  else:
    geometry = 'nonlinear'

  return geometry, symmetrynumber

geometry_freedom_degrees = {'monoatomic':3, 'linear':5, 'nonlinear':6}

def get_thermo(atoms: ase.Atoms, calc:gpaw.GPAW = None, charge:int = 0) -> ThermoChem:
  geometry, symmetrynumber = get_geometry_symmetrynumber(atoms)

  if calc is None:
    calc = gpaw.GPAW(
      mode='lcao', 
      xc='LDA', # PBE
      occupations={'name': 'fermi-dirac', 'width': 0.1}, # for singlet O2
      h=0.2, 
      charge=charge,
      symmetry='off', 
      txt=None)
  else:
    assert charge is None
  
  atoms.center(vacuum=5)
  atoms.calc = calc
  opt = LBFGSLineSearch(atoms)
  opt.run(fmax=0.01)
  
  heavy_atoms = list(np.argwhere(atoms.numbers!=1).ravel()) # indices of non-hydrogens
  if len(heavy_atoms) > 1:
    vib = Vibrations(atoms, indices=heavy_atoms, name=atoms.get_chemical_formula("reduce"))
    vib.clean()
    vib.run()
    vib.summary()
    
    n_dof = geometry_freedom_degrees[geometry]
    
    for mode in range(n_dof, len(heavy_atoms) * 3):
      vib.write_mode(mode)
    
    vib_energies = vib.get_energies()
  else:
    vib_energies = []
  
  thermo = IdealGasThermo(vib_energies, geometry=geometry, 
                        potentialenergy=atoms.get_potential_energy(), 
                        atoms=atoms, symmetrynumber=symmetrynumber, 
                        spin=round(atoms.get_magnetic_moment()/2,1))
  
  return thermo
