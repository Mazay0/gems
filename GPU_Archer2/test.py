import torch

print (torch.cuda.is_available())


from ase import build
from mace.calculators import MACECalculator

atoms = build.molecule('H2O')
calc = MACECalculator(model_paths="/work/e05/e05/pvste05/gpu/ACE_MPtrj_2022.9.model", dispersion=True, default_dtype="float64", device='cuda')
atoms.calc = calc
print(atoms.get_potential_energy())

