#!/bin/sh


module purge
module load load-epcc-module
module load epcc-setup-env
module load pytorch/1.13.1-gpu
module list


export VEDIR="/work/e05/e05/$USER/vegputorch"

python3 -m venv $VEDIR
. $VEDIR/bin/activate
python3 -m pip install --no-user --upgrade certifi
python3 -m pip install --no-user scipy==1.13.1 ase==3.22.1
python3 -m pip install --no-user mace-torch torch_dftd
python3 -m pip install --no-user jupyter

# model URLs from here: https://github.com/ACEsuit/mace/blob/2d89108d4ec27ada6ae7e63b70f79c63ed1007e7/mace/calculators/foundations_models.py#L64
wget --content-disposition  https://tinyurl.com/46jrkm3v
wget --content-disposition  https://tinyurl.com/5yyxdm76
wget --content-disposition  https://tinyurl.com/5f5yavf3

