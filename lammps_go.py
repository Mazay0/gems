import os 

os.environ['ASE_LAMMPSRUN_COMMAND']=f'/usr/bin/lmp'

import sys, numpy as np
from ase import Atoms, units
from ase.build import fcc111
from ase.calculators.lammpsrun import LAMMPS
from ase.data import atomic_numbers, chemical_symbols, atomic_masses
from itertools import combinations_with_replacement

def g_mol_mass(element):
  return atomic_masses[atomic_numbers[element]]/(units.kg/(1000.*units.mol))

def zbl_pair_coeffs(specorder):
  return [f'{i1+1} {i2+1} {atomic_numbers[specorder[i1]]} {atomic_numbers[specorder[i2]]}' for i1, i2 in combinations_with_replacement(range(len(specorder)), 2)]
    

def make_lammps_zbl(atoms):
  specorder = list(set(atoms.symbols))
  params = {}
  params['pair_style']='zbl 3.0 4.0'
  params['pair_coeff']=zbl_pair_coeffs(specorder)
  params['mass'] = [f'{i+1} {g_mol_mass(e)}' for i,e in enumerate(specorder)]
  #params['minimize'] = '1.0e-7 1.0e-2 1000 10000'
  params['run'] = 1
  calc = LAMMPS(specorder=specorder, keep_tmp_files=True, parameters=params, files=[], label=f'test_', tmp_dir = f'tmp_lammps', write_velocities=True, verbose=True)
  return calc
  


a = Atoms('Ar2',[[0,0,0], [2,0,0]])
a.calc = make_lammps_zbl(a)
a.get_potential_energy()

for x in np.arange(2.5, 4.5, 0.02):
  a.positions[1,0] = x
  print (x, a.get_potential_energy(), a.get_forces().ravel())


a = fcc111('Au', size=(1,1,4), vacuum=20)
a.pbc=True
a.calc = make_lammps_zbl(a)
print (a.get_potential_energy(), a.get_forces().ravel(), a.get_stress().ravel())
