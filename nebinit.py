from ase.build import molecule
from ase.visualize import view
from ase.neb import NEB
import numpy as np, sys, itertools

def reorder_homogenous(s, f):
  weghted_permutations = [(np.linalg.norm(s.positions - f[p].positions), p) for p in itertools.permutations(range(len(f)))]
  sorted_permutations = sorted(weghted_permutations, key=lambda dp: dp[0])
  d_min,p_best = sorted_permutations[0]
  return f[p_best]

def reorder(s, f):
  nums = set(s.numbers)
  for n in nums:
    fn = reorder_homogenous(s[s.numbers==n], f[f.numbers==n])
    f.positions[f.numbers==n] = fn.positions
  return f

def draft_trajectory(starting, final, n):
  final = reorder(starting, final)
  return [starting ] + [final.copy() for i in range(n-2)] + [final]

c2h4 = molecule('C2H4')
c2h2 = molecule('C2H2')
h2 = molecule('H2')

mols = [c2h4, c2h2, h2]

h2.translate([0, 0, 5])

for m in mols:
  print (m.symbols)
  np.savetxt(sys.stdout, m.positions, fmt='%9.6f')


neb = NEB(draft_trajectory(c2h4, c2h2 + h2, 10))

neb.interpolate(method='linear')

view (neb.images)
