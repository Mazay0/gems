#!/bin/sh 

SLURM_JID=$(sbatch --parsable submission.gpu run-jup.py)
echo "Waiting job $SLURM_JID to start ..."
until [ -e log.$SLURM_JID ]; do sleep 1; done
SLURM_HOST=$(squeue -hj $SLURM_JID -o '%N')
echo "Run tunel to the node:  ssh -vNL 8888:$SLURM_HOST:23737 $USER@hawklogin.cf.ac.uk" 
until [  -n "$JUPYTER_TOKEN" ]; do JUPYTER_TOKEN=`cat log.$SLURM_JID | awk -F = -- '/token/{print $2}' | head -n 1`; done
echo "Connect to Jupyter: http://127.0.0.1:8888/tree?token=$JUPYTER_TOKEN"

