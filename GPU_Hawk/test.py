import ssl
ssl._create_default_https_context = ssl._create_unverified_context


import numpy as np
print (np)

from mace.calculators import mace_mp
from ase import build

atoms = build.molecule('H2O')
calc = mace_mp(model="large", dispersion=True, default_dtype="float64", device='cuda')
atoms.calc = calc
print(atoms.get_potential_energy())
