# Running custom Python script

```
sbatch submission.gpu test.py
```

# Launching Jupyter on a GPU node

```
./run-gpu-jup.sh
```

