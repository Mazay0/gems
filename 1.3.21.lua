help([[
asi4py-aims 1.3.21
=============
A Python environment (based on PrgEnv-gnu and cray-python/3.10.10) that provides asi4py 1.3.21 (https://pvst.gitlab.io/asi/index.html).
The environment also sets environment variable ASI_LIB_PATH to the FHI-aims shared library
and AIMS_SPECIES_DIR to defaults_2020/tight.
* To rebuild the FHI-aims library read, edit, and run /work/e05/e05-files-log/shared/software/ASI/src/make-libaims.sh
* To rebuild the Python environment read, edit, and run /work/e05/e05-files-log/shared/software/ASI/make-ve.sh

To test the module:
cd /work/e05/e05-files-log/shared/software/ASI/src/asi/tests/testcases/
MPIEXEC="srun -n  2 --account=e05-surfin-log -p standard -q short --time=0:20:0"  ./run_tests.sh test_e.py.aims


  Installed by: Pavel Stishenko, Cardiff University
  Date: 27 January 2025
]])

load("PrgEnv-gnu")
load("cray-python/3.10.10")
load("libfabric")


local ASI_DIR = "/work/e05/e05-files-log/shared/software/ASI/"
local ASI_LIB_DIR = pathJoin(ASI_DIR,"install/lib")
local ASI_LIB_PATH = pathJoin(ASI_LIB_DIR,"libaims.so")
local AIMS_SPECIES_DIR = pathJoin(ASI_DIR,"src/FHIaims/species_defaults/defaults_2020/tight")

local ve310_activate = pathJoin(ASI_DIR,"ve310/bin/activate")

source_sh("bash", ve310_activate)

prepend_path("LD_LIBRARY_PATH", ASI_LIB_DIR)
setenv("ASI_LIB_PATH", ASI_LIB_PATH)
setenv("AIMS_SPECIES_DIR", AIMS_SPECIES_DIR)


family("TAMM")
