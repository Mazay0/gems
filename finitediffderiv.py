import numpy as np, sys
from scipy.special import factorial

class FiniteDifference:
  '''Finite difference derivatives calculator 
    **Example**
    ::
      X = np.linspace(-6, 6, 200)
      D = FiniteDifference([ -1, 0, 1, 3]) # operator
      Y = D(np.sin,X, d=[0,1,2], h=1e-3)
  '''
  def __init__(self, stencil = [-1, 0, 1]):
    '''
      stencil: list
        Stencil - a list factors to calculate shifts from the differentiation 
        point where the differentiated function will be evaluated. For example,
        a central finite difference stencil is `[-1, 0, 1]`. Can be asymmetric,
        with non-integers elements, without 0, for example: `[-1/3, 2/3]`. 
        Stencil factors will be multiplied by differentiation step `h` to 
        get points where the differentiated funtion will be evaluated.
    '''
    self.stencil = np.array(stencil)
    N = self.stencil.shape[0]
    p = np.arange(N)
    A = np.linalg.inv(self.stencil[None, :] ** p[:, None])
    A[np.abs(A) < 1e-14] = 0 # for skipping of stencils with zero coeffitients
    self.coefs = A * factorial(np.arange(N))

  def __call__(self, f, x, d=[1], h=1e-6):
    '''Returns derivatives of requested orders.
      If for all requested derivatives some stencil point is not used,
      the function `f` will not be computed in that point to save CPU time.
      For example, for a central finite difference stencil `[-1, 0, 1]` the 
      central point is not used for derivatives of odd orders.

      f: function
        Function to differentiate. Expected to take a single vector argument, 
        and to return a vector of results of the same size.
      x: 1d array of shape (n,)
        Points where derivatives will be evaluated
      d: list of m integers or array of shape (m,)
        Orders of derivatives to be evaluated. Can list orders from 0-th to 
        `N-1`-th, where `N` is a stencil size.
      h: float
        Numerical differentiation step

      Returns: array of shape (n,m), where n - number of points, 
        m - number of requested derivatives. Or 1-d array of size (n,) if only
        one derivative is requested.
    '''
    coefs = self.coefs[:, d]
    stencil_mask = np.any(coefs!=0, axis=1) # filter-out unused stencil points
    X = h * self.stencil[None, stencil_mask] + x[:, None]
    Y = (f(X) @ coefs[stencil_mask, :]) / np.power(h, d)
    return Y[:,0] if len(d) == 1 else Y


x = np.linspace(-6, 6, 200)
yp = FiniteDifference([ -1, 0, 1, 3])(np.sin,x, d=[0,1,2], h=1e-3)

import matplotlib.pylab as plt
plt.plot(x, yp)
plt.show()


