import numpy as np, sys

class gromacs_switch:
  def __init__(self,  r1, rcut, f, fp, fpp):
    self.r1 = r1
    self.A = (-3*fp  + (rcut - r1)*fpp) / ((rcut - r1)**2)
    self.B = (2*fp  - (rcut - r1)*fpp) / ((rcut - r1)**3)
    self.C = -f + 0.5*(rcut - r1)*fp - (1./12.) * ((rcut - r1)**2)* fpp
  
  
  def __call__(self, r):
    r3 = (r - self.r1)**3
    r4 = (r - self.r1)**4
    S = (self.A/3) * r3 + (self.B/4) * r4 + self.C
    
    try:
      S[r < self.r1] = self.C
      return S
    except TypeError as err:
      return C if r < self.r1 else S

class gromacs_cutoff:
  def __init__(self, func, r1, rcut, dx=1e-6):
    self.func=func
    self.r1 = r1
    self.rcut = rcut
    f = func(rcut)
    fp = (f - func(rcut - dx)) / dx
    fpp = (f - 2*func(rcut - dx) + func(rcut - 2*dx)) / (dx**2)
    self.switch = gromacs_switch(r1, rcut, f, fp, fpp)
    
  def __call__(self, x):
    x = np.atleast_1d(x)
    mask = x<self.rcut
    x1 = x[mask]
    y = self.func(x1) + self.switch(x1)
    res = np.zeros(x.shape, dtype = y.dtype)
    res[mask] = y
    return res

class ZBL:
  w1 = np.array([-3.19980, -0.94229, -0.40290, -0.20162])
  w2 = np.array([0.18175, 0.50986, 0.28022, 0.02817])
  
  def __init__(self, z1, z2):
    eps0 = 55.26349406 * 1e-4
    self.zz = z1 * z2 / (4 * np.pi * eps0)
    self.a = 0.46850 / (z1**0.23 + z2**0.23)

  @classmethod
  def phi(cls, x):
    x = np.atleast_1d(x)
    return np.sum(np.exp(x[:, None] * cls.w1[None, :]) * cls.w2[None, :], axis=1)

  def __call__(self, r):
    return (self.zz / r) * self.phi(r/self.a)


import matplotlib.pyplot as plt
fig = plt.figure()
ax1 = fig.add_axes([0.1, 0.1, 0.9, 0.9]) # main axes


r = np.arange(2.5, 4.5, 0.02)
zbl = ZBL(18, 18)
zblc = gromacs_cutoff(zbl, 3.0, 4.0)
E = zbl(r)
Ec = zblc(r)

zbl(3.0)
zblc(3.0)

np.savetxt(sys.stdout, np.array([r, E, Ec]).T)

ax1.plot(r, E)
ax1.plot(r, Ec)
ax1.grid(True)
plt.show()
sys.exit(0)

X = np.logspace(-0.5, 0.5, 30)
print (X)
Y = func(X)
print (Y)

func_cut = cutoff_wrapper(func, 1.5, 3)
print (func_cut(X))



    

ax1.plot(X, Y)
ax1.plot(X, func_cut(X))
ax1.plot(X, func_cut.switch(X))
ax1.grid(True)
plt.show()
