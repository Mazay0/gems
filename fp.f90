! -ffpe-trap=invalid,zero,overflow,underflow,denormal

program helloworld
integer :: i
real*8 :: x = 1.0d0

! underflow
!print *,'exp(-2900)', exp(x - x -2900.0d0)

! -ffpe-trap=zero
!print *,'log(0)', log(x - x)

! -ffpe-trap=denormal or -ffpe-trap=underflow 
do i=1,10000
  print *, i, x
  x = x * 0.5d0
enddo


end program helloworld
